package pl.mroziqella.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.mroziqella.ConfigTest;
import pl.mroziqella.command.city.domian.CityFacade;
import pl.mroziqella.command.city.dto.City;
import pl.mroziqella.command.weather.domain.WeatherFacade;

/**
 * Created by Mroziqella on 28.04.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigTest.class)
@TestPropertySource(locations = "classpath:test.properties")
public class IntegrationTest {

    @Autowired
    private CityFacade cityFacade;
    @Autowired
    private WeatherFacade weatherFacade;



    @Test
    public void importTest(){
        cityFacade.importCities(getClass().getClassLoader().getResource("city_list_short.txt").toString());

        City cityQuery = cityFacade.findById(1283240L);
        weatherFacade.importForCity(cityQuery.getId());
    }


}
