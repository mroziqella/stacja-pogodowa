package pl.mroziqella.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * Created by Mroziqella on 28.04.2017.
 */
@Configuration
class IntegrationConfigTest {

    @Bean
    Boolean cleanUp(MongoTemplate mongoTemplate) {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.getCollection(collectionName).drop();
            }
        }
        return true;
    }
}
