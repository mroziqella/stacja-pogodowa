package pl.mroziqella;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * Created by Mroziqella on 15.04.2017.
 */
@Configuration
@ComponentScan
@TestPropertySource("test.properties")
public class ConfigTest {

    @Value("${api.key}")
    private String apiKey;

    @Bean
    String apiKey(){
        return apiKey;
    }

    @Bean
    @Profile("unity")
    public RestTemplate restTemplate1() {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();

        Proxy proxy= new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.0.4", 3128));
        requestFactory.setProxy(proxy);

        return new RestTemplate(requestFactory);
    }

    @Bean
    @Profile("home")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }



}
