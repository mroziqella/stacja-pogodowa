package pl.mroziqella.command.weather.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import pl.mroziqella.command.weather.domain.WeatherFacade;
import pl.mroziqella.command.weather.domain.WeatherRepository;
import pl.mroziqella.command.weather.domain.WeatherService;

/**
 * Created by Mroziqella on 27.04.2017.
 */
@Configuration
public class ConfigWeatherTest {

    @Bean
    WeatherFacade weatherFacade(WeatherService weatherService){
        return new WeatherFacade(weatherService);
    }

    @Bean
    WeatherService weatherService(WeatherRepository weatherRepository, RestTemplate restTemplate, String apiKey){
        return new WeatherService(weatherRepository,restTemplate,apiKey);
    }
    @Bean
    WeatherQueryFacade weatherQueryFacade(WeatherRepository weatherRepository) {
        return new WeatherQueryFacade(weatherRepository);
    }
}
