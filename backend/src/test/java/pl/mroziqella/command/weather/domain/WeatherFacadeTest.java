package pl.mroziqella.command.weather.domain;

import org.assertj.core.util.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.mroziqella.command.city.dto.City;
import pl.mroziqella.command.weather.dto.Weather;

import java.net.InetSocketAddress;
import java.net.Proxy;

import static org.mockito.Mockito.*;

/**
 * Created by Mroziqella on 26.04.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherFacadeTest {

    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private RestTemplate restTemplate;

    private WeatherFacade weatherFacade;
    SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
    @Before
    public void init(){
        Proxy proxy= new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.0.4", 3128));
        requestFactory.setProxy(proxy);

        weatherFacade =
                new WeatherFacade(new WeatherService(weatherRepository,new RestTemplate(requestFactory),"df27549674c780361031a2efb3b39113"));
    }

    @Test
    public void importWeatherForCity(){
        weatherFacade.importForCity(2643743L);

        verify(weatherRepository).save(any(Weather.class));
    }

    @Test(expected=HttpClientErrorException.class)
    public void importAccessTest(){
        new WeatherFacade(new WeatherService(weatherRepository,new RestTemplate(requestFactory),"df274f549674c780361031a2efb3b39113"))
                .importForCity(2643743);
    }

    @Test
    public void importForCitisTest(){
        when(restTemplate.getForObject(anyString(),any())).thenReturn(new Weather());
        WeatherService weatherService = new WeatherService(weatherRepository, restTemplate, "");
        WeatherFacade weatherFacade = new WeatherFacade(weatherService);

        weatherFacade.importForCities(Sets.newLinkedHashSet(new City(1l,"A"),new City(2l,"B"),new City(3l,"C")));

        
        verify(weatherRepository,times(3)).save(any(Weather.class));
    }
}