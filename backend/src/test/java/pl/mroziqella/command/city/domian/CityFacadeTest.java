package pl.mroziqella.command.city.domian;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.mroziqella.command.city.dto.City;
import pl.mroziqella.command.file.domain.FileFacade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by Mroziqella on 15.04.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CityFacadeTest {

    @Mock
    private FileFacade fileFacade;
    @Mock
    private CityRepository cityRepository;


    private CityFacade cityFacade;

    @Before
    public void init() {
        List<String> list = new ArrayList<>();
        list.add("819827	Razvilka	55.591667	37.740833	RU");
        list.add("524901	Moscow	55.752220	37.615555	RU");
        list.add("1271881	Firozpur Jhirka	27.799999	76.949997	IN");
        list.add("1283240	Kathmandu	27.716667	85.316666	NP");



        when(fileFacade.readTSV(any())).thenReturn(list);
        when(cityRepository.findByCountry("RU"))
                .thenReturn(Arrays.asList(new City(1L,"Moscow","23.21","44.32","RU"),
                        new City(2L,"Murmans","33.21","43.32","RU")
                        ));
        when(cityRepository.findByCountry("PL"))
                .thenReturn(Collections.singletonList(new City(3L, "Ciechocinek", "13.21", "64.32", "PL")
                ));
        when(cityRepository.save(anyList())).thenReturn(null);
        cityFacade =new CityFacade(new CityService(new CityMapper(), fileFacade, cityRepository),cityRepository);
    }

    @Test
    public void importTest() {


        cityFacade.importCities("test");

        verify(cityRepository, only()).save(anyList());

    }

    @Test
    public void findByCountryRUTest(){
        List<City> cities = cityFacade.findByCountryCode("RU");

        List<City> cityListExcpect = Arrays.asList(new City(1L, "Moscow", "23.21", "44.32", "RU"),
                new City(2L, "Murmans", "33.21", "43.32", "RU"));

        Assert.assertArrayEquals(cityListExcpect.toArray(),cities.toArray());

    }

    @Test
    public void findByCountryPLTest(){
        List<City> cities = cityFacade.findByCountryCode("PL");

        List<City> cityListExcpect = Collections.singletonList(new City(3L, "Ciechocinek", "13.21", "64.32", "PL"));

        Assert.assertArrayEquals(cityListExcpect.toArray(),cities.toArray());

    }

}
