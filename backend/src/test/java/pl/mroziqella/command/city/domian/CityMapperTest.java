package pl.mroziqella.command.city.domian;

import org.junit.Before;
import org.junit.Test;
import pl.mroziqella.command.city.domian.CityMapper;
import pl.mroziqella.command.city.dto.City;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Mroziqella on 15.04.2017.
 */
public class CityMapperTest {
    private final List<String> list =  new ArrayList<>();
    private final CityMapper cityMapper  = new CityMapper();
    @Before
    public void init(){
        list.add("819827	Razvilka	55.591667	37.740833	RU");
        list.add("524901	Moscow	55.752220	37.615555	RU");
        list.add("1271881	Firozpur Jhirka	27.799999	76.949997	IN");
        list.add("1283240	Kathmandu	27.716667	85.316666	NP");

    }

    @Test
    public void parseListToCity(){

        List<City> cities =cityMapper.parseToCity(list);

        assertEquals(4,cities.size());
    }

    @Test
    public void  parseListToCityEqObject(){
        List<City> cityListExpected = new ArrayList<>();
        cityListExpected.add(new City(819827L,"Razvilka","55.591667","37.740833","RU"));
        cityListExpected.add(new City(524901L,"Moscow","55.752220","37.615555","RU"));
        cityListExpected.add(new City(1271881L,"Firozpur Jhirka","27.799999","76.949997","IN"));
        cityListExpected.add(new City(1283240L,"Kathmandu","27.716667","85.316666","NP"));


        List<City> cities = cityMapper.parseToCity(list);


        assertArrayEquals(cityListExpected.toArray(),cities.toArray());

    }


}
