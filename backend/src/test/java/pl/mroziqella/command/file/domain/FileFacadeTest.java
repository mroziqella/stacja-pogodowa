package pl.mroziqella.command.file.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Mroziqella on 15.04.2017.
 */

public class FileFacadeTest {
    private final FileFacade fileFacade =  new FileFacade(new FileService());
    @Test
    public void readFromURLTSV() {
        List<String> strings = fileFacade.readTSV(getClass().getClassLoader().getResource("city_list.txt").toString());

        assertNotNull(strings);
        assertNotEquals(new ArrayList<String>(), strings);

        assertEquals(2308,strings.size());

    }




}



