package pl.mroziqella.command.file.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Created by Mroziqella on 15.04.2017.
 */
@Configuration
class ConfigFile {

    @Bean
    FileFacade fileFacade(FileService fileService){
        return new FileFacade(fileService);
    }

    @Bean
    FileService fileService(){
        return new FileService();
    }
}
