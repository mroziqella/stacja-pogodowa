package pl.mroziqella.command.weather.domain;

import org.springframework.web.client.RestTemplate;
import pl.mroziqella.command.city.dto.City;
import pl.mroziqella.command.weather.dto.Weather;



import java.util.Set;
import java.util.logging.Logger;

/**
 * Created by Mroziqella on 15.04.2017.
 */
class WeatherService {

    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private final WeatherRepository weatherRepository;
    private final RestTemplate restTemplate;
    private final String apiKey;

    public WeatherService(WeatherRepository weatherRepository, RestTemplate restTemplate, String apiKey) {
        this.weatherRepository = weatherRepository;
        this.restTemplate = restTemplate;
        this.apiKey = apiKey;
    }

    public void importForCity(long id) {
        String url = "http://api.openweathermap.org/data/2.5/weather?id="+id+"&appid="+ apiKey+"&units=metric";
        Weather weather = restTemplate.getForObject(url, Weather.class);
        weather.setCityId(id);
        weatherRepository.save(weather);
    }

    public void importForCities(Set<City> cities) {
        for(City c: cities){
            logger.info("Import for "+c.getName());
            importForCity(c.getId());
        }
    }
}
