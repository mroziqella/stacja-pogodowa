package pl.mroziqella.command.weather.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.Date;
import java.util.Map;

/**
 * Created by Mroziqella on 26.04.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = {"id"})
@Document(collection = "weather")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Weather {

    @JsonIgnore
    private Long cityId;
    @JsonProperty("name")
    private String city;
    private Map<String,Double> wind;
    private Map<String,Double> main;
    private Map<String,Double> coord;
    @JsonProperty("dt")
    @Setter(AccessLevel.NONE)
    private Date forecastDate;

    public Weather(String city) {
        this.city = city;
    }


    public void setForecastDate(long forecastDate) {
        Instant instant = Instant.ofEpochSecond( forecastDate );
        this.forecastDate = Date.from( instant );
    }
}
