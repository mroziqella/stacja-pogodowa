package pl.mroziqella.command.weather.domain;

import org.springframework.data.domain.Sort;
import pl.mroziqella.command.weather.dto.Weather;

/**
 * Created by Mroziqella on 02.05.2017.
 */
public class WeatherQueryFacade {
    private final WeatherRepository weatherRepository;

    public WeatherQueryFacade(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    public Weather getById(Long id){
        return weatherRepository.findByCityId(id,new Sort(Sort.Direction.DESC,"_id"));
    }


}
