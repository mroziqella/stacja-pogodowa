package pl.mroziqella.command.weather.domain;


import pl.mroziqella.command.city.dto.City;


import java.util.Set;

/**
 * Created by Mroziqella on 15.04.2017.
 */
public class WeatherFacade {

    private final WeatherService weatherService;

    public WeatherFacade(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    public void importForCity(long id) {
        weatherService.importForCity(id);
    }

    public void importForCities(Set<City> cities){
        weatherService.importForCities(cities);
    }
}
