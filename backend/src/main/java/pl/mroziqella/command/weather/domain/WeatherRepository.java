package pl.mroziqella.command.weather.domain;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import pl.mroziqella.command.weather.dto.Weather;

/**
 * Created by Mroziqella on 26.04.2017.
 */
interface WeatherRepository extends MongoRepository<Weather,String>{
    Weather findByCityId(Long cityId,Sort sort);
}
