package pl.mroziqella.command.global;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by Mroziqella on 15.04.2017.
 */
@ControllerAdvice
class AdviceController {

    @ExceptionHandler(value = Exception.class)
    private String exeption(){
        return "500";
    }

}
