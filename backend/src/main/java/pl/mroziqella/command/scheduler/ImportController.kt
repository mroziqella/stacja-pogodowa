package pl.mroziqella.command.scheduler

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import pl.mroziqella.command.scheduler.ImportService;

/**
 * Created by Kamil on 2017-05-15.
 */


@Controller
@RequestMapping("/api/import")
internal class ImportController{

    @Autowired
    private lateinit var importService:ImportService

    @RequestMapping("/weather")
    fun ImportWeather(){
        importService.importWeather()
    }

}