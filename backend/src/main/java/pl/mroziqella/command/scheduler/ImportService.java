package pl.mroziqella.command.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.mroziqella.command.city.domian.CityFacade;

import pl.mroziqella.command.city.dto.City;
import pl.mroziqella.command.weather.domain.WeatherFacade;

import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Mroziqella on 29.04.2017.
 */
@Service
class ImportService {

    private final CityFacade cityFacade;
    private final WeatherFacade weatherFacade;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    ImportService(CityFacade cityFacade, WeatherFacade weatherFacade) {
        this.cityFacade = cityFacade;
        this.weatherFacade = weatherFacade;
        logger.info("Init import - CITY");
        cityFacade.importCities(getClass().getClassLoader().getResource("city_list_short.txt").toString());
    }


     @Scheduled(cron = "0 0 * * * *")
     void importWeather() throws InterruptedException {
         try {
             logger.info("Import start - WeatherComponent");
             List<City> listCities = cityFacade.findAll();
             weatherFacade.importForCities(new HashSet<>(listCities));
             logger.info("Import finish - WeatherComponent");
         }catch(Exception ex){
             Thread.sleep(400);
             importWeather();
         }
    }


}
