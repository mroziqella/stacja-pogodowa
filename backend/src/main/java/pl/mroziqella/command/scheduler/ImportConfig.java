package pl.mroziqella.command.scheduler;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Mroziqella on 29.04.2017.
 */

@Configuration
@EnableScheduling
class ImportConfig {
}
