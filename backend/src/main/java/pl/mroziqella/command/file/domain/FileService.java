package pl.mroziqella.command.file.domain;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mroziqella on 15.04.2017.
 */
class FileService {

     List<String> readDocument(String url) {
        BufferedReader in;
        try {
            in = loadFile(url);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return createListFromRowFile(in);
    }

    private BufferedReader loadFile(String url) throws Exception {
        URL object = new URL(url);
        return new BufferedReader(new InputStreamReader(object.openStream()));
    }

    private List<String> createListFromRowFile(BufferedReader in) {
        List<String> listCity = new ArrayList<>();
        try {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                listCity.add(inputLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        listCity.remove(0);
        return listCity;
    }
}
