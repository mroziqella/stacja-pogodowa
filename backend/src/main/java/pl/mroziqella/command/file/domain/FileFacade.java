package pl.mroziqella.command.file.domain;


import java.util.List;

/**
 * Created by Mroziqella on 15.04.2017.
 */
public class FileFacade {
    private final FileService fileService;

    public FileFacade(FileService fileService) {
        this.fileService = fileService;
    }

    public List<String> readTSV(String url){
        return fileService.readDocument(url);
    }

}
