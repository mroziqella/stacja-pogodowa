package pl.mroziqella.command.city.domian;

import pl.mroziqella.command.city.dto.City;


import java.util.List;

/**
 * Created by Mroziqella on 15.04.2017.
 */
public class CityFacade {

    private final CityService cityService;
    private final CityRepository cityRepository;


    public CityFacade(CityService cityService, CityRepository cityRepository) {
        this.cityService = cityService;
        this.cityRepository = cityRepository;
    }

    public void importCities(String url){
        cityService.importCity(url);
    }

    public List<City> findByCountryCode(String countryCode) {
        return cityService.findByCountryCode(countryCode);
    }


    public City findById(long id) {
        return cityRepository.findOne(id);
    }

    public List<City> findAll() {
        return cityRepository.findAll();
    }

}
