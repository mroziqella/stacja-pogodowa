package pl.mroziqella.command.city.domian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mroziqella.command.weather.domain.WeatherQueryFacade;
import pl.mroziqella.command.weather.dto.Weather;

/**
 * Created by Mroziqella on 02.05.2017.
 */
@RestController
@RequestMapping("/api/weather")
class WeatherController {

    @Autowired
    private WeatherQueryFacade weatherQueryFacade;

    @RequestMapping("/getByCityId/{id}")
    private Weather getWeatherByCityId(@PathVariable Long id) {
        return weatherQueryFacade.getById(id);
    }

}