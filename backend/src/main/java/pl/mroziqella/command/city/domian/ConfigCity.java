package pl.mroziqella.command.city.domian;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.mroziqella.command.file.domain.FileFacade;


/**
 * Created by Mroziqella on 15.04.2017.
 */
@Configuration
class ConfigCity {

    @Bean
    CityFacade cityFacade(CityService cityService,CityRepository cityRepository){
        return new CityFacade(cityService,cityRepository);
    }
    @Bean
    CityService cityService(CityMapper cityMapper, FileFacade fileFacade, CityRepository cityRepository) {
        return new CityService(cityMapper,fileFacade,cityRepository);
    }


    @Bean
    CityMapper cityMapper(){
        return new CityMapper();
    }


}
