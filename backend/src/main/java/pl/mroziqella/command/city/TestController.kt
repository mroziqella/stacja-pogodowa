package pl.mroziqella.command.city

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.mroziqella.command.city.domian.CityFacade

import pl.mroziqella.command.city.dto.City


/**
 * Created by Mroziqella on 09.05.2017.
 */

@RestController
@RequestMapping("/api/city")
private class TestController{

    @Autowired
    private lateinit var cityFacade: CityFacade
    
    @RequestMapping("/gets")
    private fun test(): List<City> {
        return cityFacade.findAll()
    }
}