package pl.mroziqella.command.city.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Mroziqella on 15.04.2017.
 */


@Setter
@Getter
@NoArgsConstructor
@Document(collection = "City")
@EqualsAndHashCode(exclude = {"id"})
public class City {

    @Id
    @Indexed
    private Long id;
    private String name;
    private String lat;
    private String lon;
    @Indexed
    private String country;

    public City(Long id, String name, String lat, String lon, String country) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.country = country;
    }

    public City(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
