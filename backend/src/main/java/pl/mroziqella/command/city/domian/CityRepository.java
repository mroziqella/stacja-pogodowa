package pl.mroziqella.command.city.domian;


import org.springframework.data.mongodb.repository.MongoRepository;
import pl.mroziqella.command.city.dto.City;

import java.util.List;

/**
 * Created by Mroziqella on 15.04.2017.
 */
interface CityRepository extends MongoRepository<City,Long> {
        List<City> findByCountry(String country);
}
