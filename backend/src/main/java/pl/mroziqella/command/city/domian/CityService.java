package pl.mroziqella.command.city.domian;

import pl.mroziqella.command.city.dto.City;
import pl.mroziqella.command.file.domain.FileFacade;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mroziqella on 15.04.2017.
 */
class CityService {

    private final CityMapper cityMapper;
    private final FileFacade fileFacade;
    private final CityRepository cityRepository;

    CityService(CityMapper cityMapper, FileFacade fileFacade, CityRepository cityRepository) {
        this.cityMapper = cityMapper;
        this.fileFacade = fileFacade;
        this.cityRepository = cityRepository;
    }

    void importCity(String url){
        List<City> cities = cityMapper.parseToCity(fileFacade.readTSV(url));
        List<City> plCitises = cities.stream().filter(r -> r.getCountry().equals("PL")).collect(Collectors.toList());
        cityRepository.save(plCitises);
    }

    public List<City> findByCountryCode(String countryCode) {
        return cityRepository.findByCountry(countryCode);
    }
}
