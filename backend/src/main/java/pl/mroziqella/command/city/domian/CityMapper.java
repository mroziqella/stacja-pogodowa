package pl.mroziqella.command.city.domian;

import pl.mroziqella.command.city.dto.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mroziqella on 15.04.2017.
 */
class CityMapper {

     List<City> parseToCity(List<String> list) {
        List<City> cities = new ArrayList<>();
        list.forEach(c->{
            String[] tmp=c.split("\t");
            cities.add(new City(Long.parseLong(tmp[0]),tmp[1],tmp[2],tmp[3],tmp[4]));
        });
        return cities;
    }

}
