package pl.mroziqella.command.city;

/**
 * Created by Mroziqella on 01.05.2017.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mroziqella.command.city.domian.CityFacade;
import pl.mroziqella.command.city.dto.City;

import java.util.List;

@RestController
@RequestMapping("/api/city")
class CityController {

    @Autowired
    private CityFacade cityFacade;

    @RequestMapping("/getAll")
    private List<City> getAll(){
        return cityFacade.findAll();
  }
}
