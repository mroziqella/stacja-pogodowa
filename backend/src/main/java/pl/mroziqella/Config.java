package pl.mroziqella;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Mroziqella on 15.04.2017.
 */
@Configuration
class Config {
    @Value("${api.key}")
    private String apiKey;

    @Bean
    @Profile("unity")
    public RestTemplate restTemplate1() {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();

        Proxy proxy= new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.0.4", 3128));
        requestFactory.setProxy(proxy);

        return new RestTemplate(requestFactory);
    }

    @Bean
    @Profile("home")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    String apiKey(){
        return apiKey;
    }


//    @Bean
//    ErrorViewResolver supportPathBasedLocationStrategyWithoutHashes() {
//        return new ErrorViewResolver() {
//            @Override
//            public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {
//                return status == HttpStatus.NOT_FOUND
//                        ? new ModelAndView("index.html", Collections.<String, Object>emptyMap(), HttpStatus.OK)
//                        : null;
//            }
//        };
//    }



}
