package pl.mroziqella

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * Created by Mroziqella on 09.05.2017.
 */
@SpringBootApplication
// This class must not be final or Spring Boot is not happy.
open class Application {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
        }
    }
}