import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {City} from "./app.domain.city";

@Injectable()
export class CityService {
  constructor(private http: Http) {
  }

  getCities(): Observable<City[]> {
    return this.http.get("/api/city/getAll")
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
