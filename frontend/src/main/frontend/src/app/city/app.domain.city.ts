/**
 * Created by Mroziqella on 01.05.2017.
 */
export class City {
 
  public id:string;
  public name: string;
  public lat: number;
  public lon: number;
  constructor() {}
}
