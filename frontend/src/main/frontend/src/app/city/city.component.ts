/**
 * Created by Mroziqella on 01.05.2017.
 */

import { Component } from '@angular/core';
import {CityService} from "./app.service.city";
import {AppComponent} from "../app.component";
import {Router} from "@angular/router";

@Component({
  selector: 'city',
  templateUrl: './city.component.html'
})

export class CityComponent{
  users;
  constructor(private cityService: CityService) {
    this.users=cityService.getCities();
  }


}
