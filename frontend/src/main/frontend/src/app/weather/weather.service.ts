import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from "rxjs";
import {Weather} from "./weather";

@Injectable()
export class WeatherService {
  constructor(private http: Http) {
  }

  getWeather(id: number):Observable<Weather> {
    return this.http.get("/api/weather/getByCityId/"+id)
      .map(res => res.json() as Weather)
  }
}
