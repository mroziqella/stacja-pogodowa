/**
 * Created by Mroziqella on 02.05.2017.
 */
export class Weather{
    name: string;
    main: any;
    coord: any;
    wind:any;
    dt:Date;
  constructor() {
    this.main = new Object();
    this.coord  = new Object();
    this.wind=new Object();
  }
}
