/**
 * Created by Mroziqella on 01.05.2017.
 */
import {Component, OnInit, Input} from '@angular/core';
import{ActivatedRoute} from "@angular/router";
import {WeatherService} from "./weather.service";
import {Weather} from "./weather";
import {City} from "../city/app.domain.city";


@Component({
  selector: 'weather',
  templateUrl: './weather.component.html',
  providers: [WeatherService]
})

export class WeatherComponent implements OnInit {
  weather: Weather = new Weather();
  
  constructor(private route: ActivatedRoute, private weatherService: WeatherService) {
  }


  ngOnInit(): void {
    this.route.params.subscribe(param =>  {
      this.getWeather(param['id']);
    });

  }

  getWeather(id: number) {
    this.weatherService.getWeather(id)
      .subscribe(
        weather => {
          this.weather = weather;
        },
        error => console.log(error)
      )
  }
}
