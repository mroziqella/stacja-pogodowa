import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import {CityService} from "./city/app.service.city";
import {WeatherComponent} from "./weather/weather.component";
import {CityComponent} from "./city/city.component";
import {MapComponent} from "./map/map.component";
import { AgmCoreModule } from 'angular2-google-maps/core';



const routes: Routes = [
  { path: 'city/:id', component: WeatherComponent, data: [{isProd: true}] },
  { path: '', component: CityComponent  },
];


@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    CityComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes, { useHash: false }),
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyDNjG3S7Vw1z7t3dI8cRyQLEO0LBPB5pVE'
      })
  ],
  providers: [CityService],
  bootstrap: [AppComponent]
})
export class AppModule {


}
